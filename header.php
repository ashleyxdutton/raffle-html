<!doctype html>
<html>
<head>
  <!-- metadata -->
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Raffle Site</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

  <!-- favicon -->
  <link rel="icon" type="image/svg+xml" href="./assets/favicon.svg">

  <!-- stylesheets -->
  <link rel="stylesheet" type="text/css" href="./assets/css/main.css">

  <!-- temp icons -->
  <script src="https://kit.fontawesome.com/8d4459eaa9.js" crossorigin="anonymous"></script>

</head>

<body data-barba="wrapper">

	<header class="siteHeader flex flex--x-between flex--y-center" style="padding:30px;border-bottom:1px solid black;">

		<a class="siteHeader__logo" href="./" style="padding:30px 75px;border:1px solid black;">Logo</a>

		<nav class="siteHeader__nav">
			<ul class="siteHeader__navList flex">
				<li class="siteHeader__navItem">
					<a class="siteHeader__navLink" href="./competitions.php">Competitions</a>
				</li>
				<li class="siteHeader__navItem">
					<a class="siteHeader__navLink" href="./how-to-win.php">How to win</a>
				</li>
				<li class="siteHeader__navItem">
					<a class="siteHeader__navLink" href="./winners.php">Winners</a>
				</li>
				<li class="siteHeader__navItem">
					<a class="siteHeader__navLink" href="./about-us.php">About us</a>
				</li>
			</ul>
		</nav>

		<div class="siteHeader__admin">
      <ul class="siteHeader__adminList flex">
        <li class="siteHeader__adminItem">
          <a class="siteHeader__login" href="./login.php"><i class="fas fa-user"></i>Login/Sign up</a>
        </li>
        <li class="siteHeader__adminItem">
          <a class="siteHeader__basket" href="#">
    				<p class="siteHeader__basketLabel"><i class="fas fa-shopping-cart"></i>£15.00</p>
    			</a>
        </li>
      </ul>

      <!-- <div class="siteHeader__basketWindow pos--abs" style="background:white;padding:10px;width:250px;right:0;">
        <ul class="siteHeader__basketList">
          <li class="siteHeader__basketItem flex">
            <img class="siteHeader__basketImage" width="50" src="./assets/images/placeholder.jpg" alt="">
            <h3 class="siteHeader__basketTitle">Audi RS7</h3>
            <p class="siteHeader__basketQuantity">3</p>
            <p class="siteHeader__basketPrice">£7.50</p>
          </li>
          <li class="siteHeader__basketItem flex">
            <img class="siteHeader__basketImage" width="50" src="./assets/images/placeholder.jpg" alt="">
            <h3 class="siteHeader__basketTitle">BMW M2.3</h3>
            <p class="siteHeader__basketQuantity">1</p>
            <p class="siteHeader__basketPrice">£2.50</p>
          </li>
          <li class="siteHeader__basketItem flex">
            <img class="siteHeader__basketImage" width="50" src="./assets/images/placeholder.jpg" alt="">
            <h3 class="siteHeader__basketTitle">Mini HB</h3>
            <p class="siteHeader__basketQuantity">2</p>
            <p class="siteHeader__basketPrice">£5.00</p>
          </li>
        </ul>
        <div class="siteHeader__basketTotal">
          <p class="siteHeader__basketTotalLabel">Total: £15.00</p>
          <a class="siteHeader__basketTotalButton" href="#">Proceed to pay</a>
        </div>
      </div> -->

		</div>

	</header>
