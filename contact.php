<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="contact">
      <h1 class="contact__heading">Contact us</h1>

      <div class="flex flex--r-nowrap">
        <form class="contact__form">
          <fieldset class="contact__fieldset">
            <label class="contact__label" for="name">Name</label>
            <input class="contact__input" type="text" name="name" value="">
          </fieldset>
          <fieldset class="contact__fieldset">
            <label class="contact__label" for="email">Email address</label>
            <input class="contact__input" type="email" name="email" value="">
          </fieldset>
          <fieldset class="contact__fieldset">
            <label class="contact__label" for="message">Message</label>
            <textarea class="contact__textarea" name="message" rows="8" cols="80"></textarea>
          </fieldset>
          <button class="contact__button button" type="button" name="button">Send message</button>
        </form>
        <h3 class="contact__address">23 Market Street, Manchester, United Kingdom, M14 5LA</h3>
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
