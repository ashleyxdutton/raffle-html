<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="dashboard">
      <div class="dashboard__header">
        <img class="dashboard__initials" src="./assets/images/placeholder.jpg" alt="">
        <h1 class="dashboard__heading">Welcome Dan</h1>
        <ul class="dashboard__nav">
          <li class="dashboard__navItem">
            <a class="dashboard__navLink" href="./dashboard-competitions.php">My Competitions</a>
          </li>
          <li class="dashboard__navItem">
            <a class="dashboard__navLink active" href="./dashboard-details.php">My Details</a>
          </li>
        </ul>
      </div>

      <div class="dashboard__details">
        <div class="dashboard__formHolder flex flex--x-between">
          <form class="dashboard__form dashboard__form--details">
            <h3 class="">Personal details</h3>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="name">Full name</label>
              <input class="dashboard__input" type="text" name="name" value="Dan Altwood">
            </fieldset>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="email">Email address</label>
              <input class="dashboard__input" type="email" name="email" value="danny@hotmail.co.uk">
            </fieldset>
            <h3 class="">Billing information</h3>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="street">Street address</label>
              <input class="dashboard__input" type="text" name="street" value="123, Castle Street">
            </fieldset>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="postcode">Post code</label>
              <input class="dashboard__input" type="text" name="postcode" value="M1 1RN">
            </fieldset>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="city">City</label>
              <input class="dashboard__input" type="text" name="city" value="Manchester">
            </fieldset>
            <button class="dashboard__button button" type="button" name="button">Save details</button>
          </form>
          <form class="dashboard__form dashboard__form--password">
            <h3 class="">Change password</h3>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="oldpass">Old password</label>
              <input class="dashboard__input" type="password" name="oldpass" value="">
            </fieldset>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="newpass">New password</label>
              <input class="dashboard__input" type="password" name="newpass" value="">
            </fieldset>
            <fieldset class="dashboard__fieldset">
              <label class="dashboard__label" for="confirmpass">Confirm new password</label>
              <input class="dashboard__input" type="password" name="confirmpass" value="">
            </fieldset>
            <button class="dashboard__button button" type="button" name="button">Change password</button>
          </form>
        </div>
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
