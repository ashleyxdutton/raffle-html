<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="featuredComp">
      <h1 class="featuredComp__heading align--center">Competitions</h1>

      <div class="flex flex--r-nowrap">
        <div class="featuredComp__content">
          <h3 class="featuredComp__subHeading">Featured competition</h3>
          <h2 class="featuredComp__compHeading">Audi RS7</h2>
          <p class="featuredComp__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <a class="featuredComp__readMore" href="./competitions-full.php">Read more</a>
          <div class="featuredComp__quantity flex flex--y-center">
            <select class="featuredComp__quantity flex flex--y-centerSelect">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="featuredComp__quantity flex flex--y-centerPrice">£0.00</p>
          </div>
          <a class="featuredComp__button button" href="#">Buy tickets</a>
        </div>
        <ul class="featuredComp__info">
          <li class="featuredComp__price"><i class="fas fa-ticket-alt"></i>£2.50 per ticket</li>
          <li class="featuredComp__time"><i class="fas fa-clock"></i>3 days 22 hours remaining</li>
          <li class="featuredComp__entries"><i class="fas fa-stopwatch-20"></i>2/5 entries used</li>
        </ul>
      </div>

      <a class="featuredComp__scroll" href="#"><i class="fas fa-arrow-down"></i>Scroll for more</a>
    </section>

    <section class="allComps">
      <h1 class="allComps__heading">All competitions</h1>

      <ul class="allComps__list flex flex--x-between">

        <li class="allComps__item">
          <h2 class="allComps__itemHeading">BMW S3</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£2.50 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>6 days 12 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>2/5 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

        <li class="allComps__item">
          <h2 class="allComps__itemHeading">Apple iMac</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£3.50 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>3 days 13 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>2/5 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

        <li class="allComps__item">
          <h2 class="allComps__itemHeading">Sony 60" TV</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£1.50 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>3 days 23 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>3/5 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

        <li class="allComps__item" style="opacity:.5;">
          <h2 class="allComps__itemHeading">Coming soon</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£0.00 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>0 days 0 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>0/0 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

        <li class="allComps__item" style="opacity:.5;">
          <h2 class="allComps__itemHeading">Coming soon</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£0.00 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>0 days 0 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>0/0 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

        <li class="allComps__item" style="opacity:.5;">
          <h2 class="allComps__itemHeading">Coming soon</h2>
          <img class="allComps__image" src="./assets/images/placeholder.jpg" alt="">
          <a class="allComps__seeFull" href="./competitions-full.php">See full listing</a>
          <ul class="allComps__info">
            <li class="allComps__price"><i class="fas fa-ticket-alt"></i>£0.00 per ticket</li>
            <li class="allComps__time"><i class="fas fa-clock"></i>0 days 0 hours remaining</li>
            <li class="allComps__entries"><i class="fas fa-stopwatch-20"></i>0/0 entries used</li>
          </ul>
          <div class="allComps__quantity flex flex--y-center">
            <select class="allComps__quantitySelect flex flex--y-center">
              <option>Ticket quantity</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
            <p class="allComps__quantityPrice flex flex--y-center">£0.00</p>
          </div>
          <a class="allComps__button button" href="#">Buy tickets</a>
        </li>

      </ul>
    </section>

  </main>

<?php include "footer.php"; ?>
