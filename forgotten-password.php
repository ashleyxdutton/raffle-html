<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="forgotten">
      <div class="forgotten__header">
        <h1 class="forgotten__heading">Forgot your password?</h1>
        <p class="forgotten__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <form class="forgotten__form">
          <input class="forgotten__input" type="email" name="email" placeholder="Email address" value="">
          <button class="forgotten__button button" type="button" name="button">Send email</button>
        </form>
      </div>
    </section>

  </main>

<?php include "footer.php"; ?>
