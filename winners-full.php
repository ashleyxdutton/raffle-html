<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="stories">
      <div class="stories__header flex flex--r-nowrap">
        <video class="stories__video" autoplay controls>
          <source src="./assets/video.mp4" type="video/mp4">
        </video>
        <div class="stories__content">
          <h2 class="stories__heading">Audi R8 Winner</h2>
          <p class="stories__date">13 - 19 Apr 2020 Competition</p>
          <h3 class="stories__name">Jamie Forshaw</h3>
          <p>Manchester, 34 years old</p>
          <p class="stories__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
      </div>
      <div class="stories__story flex flex--r-nowrap">
        <div class="stories__storyContent">
          <h2 class="stories__storyHeading">Jamie's Story</h2>
          <p class="stories__storyText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p class="stories__storyText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <img class="stories__image" src="./assets/images/placeholder.jpg" alt="">
      </div>
    </section>

  </main>

<?php include "footer.php"; ?>
