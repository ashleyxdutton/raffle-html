<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="about">
      <h1 class="about__heading align--center">About us</h1>

      <div class="flex flex--r-nowrap">
        <div class="about__content">
          <h2 class="about__title">We turn dreams into reality</h2>
          <p class="about__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p class="about__text">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <a class="about__button button" href="./competitions.php">View competitions</a>
        </div>

        <img class="about__image" src="./assets/images/placeholder.jpg" alt="">
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
