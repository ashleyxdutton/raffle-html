// GRUNT COMMANDS
module.exports = function(grunt) {

  require('jit-grunt')(grunt);

  grunt.loadNpmTasks('grunt-contrib-uglify-es');

  // CONFIGURE SETTINGS
  grunt.initConfig({

    // BASIC SETTINGS ABOUT PLUGINS
    pkg: grunt.file.readJSON('package.json'),

    // SASS PLUGINS
    sass: {
      dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed'
        },
        files: {
          'assets/css/main.css': 'assets/css/scss/main.scss'
        }
      }
    },

    // ADD PREFIXES TO MAIN.CSS AND RE-COMPRESS
    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer')
        ]
      },
      dist: {
        src: 'assets/css/main.css'
      }
    },

    // UGLIFY PLUGIN
    uglify: {
      options: {
        mangle: false,
        beautify: false
      },
      modules: {
        files: {
          'assets/js/modules.min.js': [
            'node_modules/@barba/core/dist/barba.umd.js',
            'node_modules/@barba/prefetch/dist/barba-prefetch.umd.js',
            'node_modules/gsap/dist/gsap.min.js'
            // addition libraries from node_modules or assets/js/lib go here if needed
          ]
        }
      },
      scripts: {
        files: {'assets/js/scripts.min.js': 'assets/js/src/*'}
      }
    },

    // WATCH FILES FOR CHANGES
    watch: {
      jsscripts: {
        files: ['assets/js/src/*'],
        tasks: ['uglify:scripts']
      }
    }

  });

  // DO THE TASK
  grunt.registerTask('default', ['sass']);
  grunt.registerTask('default', ['postcss:dist']);
  grunt.registerTask('default', ['uglify']);
  grunt.registerTask('default', ['watch']);
};
