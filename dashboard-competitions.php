<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="dashboard">
      <div class="dashboard__header">
        <img class="dashboard__initials" src="./assets/images/placeholder.jpg" alt="">
        <h1 class="dashboard__heading">Welcome Dan</h1>
        <ul class="dashboard__nav">
          <li class="dashboard__navItem">
            <a class="dashboard__navLink active" href="./dashboard-competitions.php">My Competitions</a>
          </li>
          <li class="dashboard__navItem">
            <a class="dashboard__navLink" href="./dashboard-details.php">My Details</a>
          </li>
        </ul>
      </div>

      <div class="dashboard__competitions">
        <table class="dashboard__table">
          <tr>
            <th>Prize Draw</th>
            <th>Ticket Quantity</th>
            <th>Total Paid</th>
            <th>Status</th>
          </tr>
          <tr>
            <td>BMW S3</td>
            <td>4</td>
            <td>£6.50</td>
            <td>3 days 22 hours left</td>
          </tr>
          <tr>
            <td>Audi R8</td>
            <td>2</td>
            <td>£3.50</td>
            <td>4 days 20 hours left</td>
          </tr>
          <tr>
            <td>Dream Holiday</td>
            <td>1</td>
            <td>£1.50</td>
            <td>Ended 22/03/2020</td>
          </tr>
          <tr>
            <td>Apple iMac</td>
            <td>5</td>
            <td>£4.00</td>
            <td>Ended 27/02/2020</td>
          </tr>
        </table>
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
