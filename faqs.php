<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="faqs">
      <div class="faqs__header">
        <h1 class="faqs__heading">Frequently Asked Questions</h1>
        <p class="faqs__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
      </div>

      <ul class="faqs__accordion">
        <li class="faqs__accordionItem">
          <p class="faqs__accordionTitle"><i class="fas fa-arrow-down"></i>Who are we?</p>
          <ul class="faqs__subAccordion">
            <li class="faqs__subAccordionItem">
              <p class="faqs__subAccordionTitle"><i class="fas fa-arrow-down"></i>Is PryzBox online only?</p>
              <p class="faqs__subAccordionText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
            </li>
            <li class="faqs__subAccordionItem">
              <p class="faqs__subAccordionTitle"><i class="fas fa-arrow-down"></i>How did PryzBox start?</p>
            </li>
          </ul>
        </li>

        <li class="faqs__accordionItem">
          <p class="faqs__accordionTitle"><i class="fas fa-arrow-down"></i>How does it work?</p>
        </li>

        <li class="faqs__accordionItem">
          <p class="faqs__accordionTitle"><i class="fas fa-arrow-down"></i>Winners and prizes</p>
        </li>
      </ul>
    </section>

  </main>

<?php include "footer.php"; ?>
