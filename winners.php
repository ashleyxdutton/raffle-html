<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="winners">
      <div class="winners__hero">
        <h1 class="winners__mainHeading"></h1>
        <div class="winners__content">
          <h2 class="winners__heading">It could be you next</h2>
          <p class="winners__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>

        <a class="winners__scroll" href="#"><i class="fas fa-arrow-down"></i>Scroll for more</a>
      </div>

      <div class="winners__stories">
        <h2 class="winners__storiesHeading">The lucky winners</h2>
        <ul class="winners__storiesList flex flex--r-nowrap flex--x-between">
          <li class="winners__storiesItem">
            <h3 class="winners__storiesName">Jamie Forshaw</h3>
            <h4 class="winners__storiesPrize">BMW S3</h4>
            <img class="winners__storiesImage" src="./assets/images/placeholder.jpg" alt="">
            <p class="winners__storiesText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            <a class="winners__storiesButton" href="./winners-full.php">Read Jamie's story</a>
          </li>
          <li class="winners__storiesItem">
            <h3 class="winners__storiesName">Gill Fountain</h3>
            <h4 class="winners__storiesPrize">Apple iMac</h4>
            <img class="winners__storiesImage" src="./assets/images/placeholder.jpg" alt="">
            <p class="winners__storiesText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            <a class="winners__storiesButton" href="./winners-full.php">Read Gills's story</a>
          </li>
          <li class="winners__storiesItem">
            <h3 class="winners__storiesName">Adam Cowen</h3>
            <h4 class="winners__storiesPrize">Sony 60" TV</h4>
            <img class="winners__storiesImage" src="./assets/images/placeholder.jpg" alt="">
            <p class="winners__storiesText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            <a class="winners__storiesButton" href="./winners-full.php">Read Adams's story</a>
          </li>
        </ul>
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
