<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="howToWin">
      <div class="howToWin__header">
        <h1 class="howToWin__heading">How to win</h1>
        <p class="howToWin__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
      </div>

      <ul class="howToWin__list flex flex--r-nowrap flex--x-between">
        <li class="howToWin__item">
          <h2 class="howToWin__itemTitle">Step 1</h2>
          <img class="howToWin__itemImage" src="./assets/images/placeholder.jpg" alt="">
          <p class="howToWin__itemText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
        </li>
        <li class="howToWin__item">
          <h2 class="howToWin__itemTitle">Step 2</h2>
          <img class="howToWin__itemImage" src="./assets/images/placeholder.jpg" alt="">
          <p class="howToWin__itemText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
        </li>
        <li class="howToWin__item">
          <h2 class="howToWin__itemTitle">Step 3</h2>
          <img class="howToWin__itemImage" src="./assets/images/placeholder.jpg" alt="">
          <p class="howToWin__itemText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
        </li>
      </ul>
    </section>

  </main>

<?php include "footer.php"; ?>
