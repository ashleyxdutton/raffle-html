// // prefetch enabled
// barba.use(barbaPrefetch);
//
//
// // fire when next page enters, before it animates in
// barba.hooks.enter(data=>{
//   // update global container variable before next page animation
//   barbaContainer = data.next.container
//   barbaNamespace = data.next.namespace
//   // fire runners, before next page animates in
//   initLoadFunctions(false)
// })
//
//
// // all transitions, fire in order, default should always be last
// barba.init({
//   debug: true,
//   timeout: 5000,
//   transitions: [
//     {
//       name: 'default',
//       leave({ current, next }) {
//
//
//
//       },
//       after({ next }) {
//
//
//
//       }
//     }
//   ]
// })
//
//
// // fires on preload and inbetween every transition
// function initLoadFunctions(initial) {
//
//   // add page class to body
//   document.body.className = barbaNamespace
//
//   // scroll to top ready for next page
//   document.body.scrollTop = 0
//   document.documentElement.scrollTop = 0
//
//   // if this is first page load, run these functions
//   if (initial) {
//
//   } else {
//
//   }
//
// }
