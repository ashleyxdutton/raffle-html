<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="terms">
      <div class="terms__header">
        <h1 class="terms__heading">Terms & Conditions</h1>
        <p class="terms__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
      </div>

      <div class="terms__content">
        <h2>1.0 Heading</h2>
        <h3>1.1 Title</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ut sem viverra aliquet eget sit amet. Nulla facilisi cras fermentum odio eu feugiat pretium. Urna nec tincidunt praesent semper feugiat nibh sed. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Nulla at volutpat diam ut venenatis tellus in. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis. Gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim. Risus ultricies tristique nulla aliquet enim. Ornare arcu dui vivamus arcu felis. Habitant morbi tristique senectus et. Semper quis lectus nulla at volutpat diam ut venenatis. Non consectetur a erat nam at lectus urna duis convallis. Tempus egestas sed sed risus pretium quam.</p>
        <h3>1.2 Title</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ut sem viverra aliquet eget sit amet. Nulla facilisi cras fermentum odio eu feugiat pretium. Urna nec tincidunt praesent semper feugiat nibh sed. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Nulla at volutpat diam ut venenatis tellus in. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis. Gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim. Risus ultricies tristique nulla aliquet enim. Ornare arcu dui vivamus arcu felis. Habitant morbi tristique senectus et. Semper quis lectus nulla at volutpat diam ut venenatis. Non consectetur a erat nam at lectus urna duis convallis. Tempus egestas sed sed risus pretium quam.</p>
        <h3>1.3 Title</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ut sem viverra aliquet eget sit amet. Nulla facilisi cras fermentum odio eu feugiat pretium. Urna nec tincidunt praesent semper feugiat nibh sed. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Nulla at volutpat diam ut venenatis tellus in. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis. Gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim. Risus ultricies tristique nulla aliquet enim. Ornare arcu dui vivamus arcu felis. Habitant morbi tristique senectus et. Semper quis lectus nulla at volutpat diam ut venenatis. Non consectetur a erat nam at lectus urna duis convallis. Tempus egestas sed sed risus pretium quam.</p>
      </div>

    </section>

  </main>

<?php include "footer.php"; ?>
