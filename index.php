<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="header align--center" style="background:lightgrey;padding:50px 0;">
      <div class="header__content">
        <h1 class="header__heading">All it takes is one ticket to change your life</h1>
        <a class="header__button button button" href="#">Hero image</a>
      </div>
    </section>

    <section class="activeComps">
      <h2 class="activeComps__heading">Active Competitions</h2>
      <div class="activeComps__wrap flex flex--r-nowrap">
        <div class="activeComps__carousel">
          <img class="activeComps__carouselImage" src="./assets/images/placeholder.jpg" alt="">
        </div>
        <div class="activeComps__content">
          <h3 class="activeComps__compHeading">Audi RS7</h3>
          <ul class="activeComps__info">
            <li class="activeComps__price"><i class="fas fa-ticket-alt"></i>£2.50 per ticket</li>
            <li class="activeComps__time"><i class="fas fa-clock"></i>3 days 22 hours remaining</li>
          </ul>
          <p class="activeComps__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <a class="activeComps__button button button" href="./competitions-full.php">View competition</a>
        </div>
      </div>
    </section>

  </main>

<?php include "footer.php"; ?>
