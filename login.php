<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="login flex flex--r-nowrap flex--x-between">
      <div class="login__signIn">
        <h1 class="login__heading">Sign in</h1>
        <form class="login__form">
          <input class="login__input" type="text" placeholder="Username" value="">
          <input class="login__input" type="password" placeholder="Password" value="">
          <a class="login__forgotten" href="./forgotten-password.php">Forgotten your password?</a>
          <a class="login__button button" href="./dashboard-competitions.php">Sign in</a>
          <!-- <button class="login__button button" type="button" name="button">Sign in</button> -->
        </form>
      </div>

      <span class="login__seperator">Or</span>

      <div class="login__signUp">
        <h1 class="login__heading">Sign up</h1>
        <form class="login__form">
          <input class="login__input" type="text" placeholder="Username" value="">
          <input class="login__input" type="email" placeholder="Email address" value="">
          <input class="login__input" type="password" placeholder="Password" value="">
          <input class="login__input" type="password" placeholder="Confirm password" value="">
          <a class="login__button button" href="./dashboard-details.php">Sign up</a>
          <!-- <button class="login__button button" type="button" name="button">Sign up</button> -->
        </form>
      </div>
    </section>

  </main>

<?php include "footer.php"; ?>
