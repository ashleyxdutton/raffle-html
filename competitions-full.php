<?php include "header.php"; ?>

<div data-barba="container" data-barba-namespace="home">
  <main class="siteContent">

    <section class="fullComp flex flex--r-nowrap flex--y-start">
      <img class="fullComp__image" src="./assets/images/placeholder.jpg" alt="">
      <div class="fullComp__content">
        <a class="fullComp__back" href="./competitions.php">< Back to competitions</a>
        <h2 class="fullComp__compHeading">Audi RS7</h2>
        <ul class="fullComp__info">
          <li class="fullComp__price"><i class="fas fa-ticket-alt"></i>£2.50 per ticket</li>
          <li class="fullComp__time"><i class="fas fa-clock"></i>3 days 22 hours remaining</li>
          <li class="fullComp__entries"><i class="fas fa-stopwatch-20"></i>2/5 entries used</li>
        </ul>
        <p class="fullComp__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <p class="fullComp__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
        <div class="fullComp__quantity flex flex--y-center">
          <select class="fullComp__quantity flex flex--y-centerSelect">
            <option>Ticket quantity</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
          </select>
          <p class="fullComp__quantity flex flex--y-centerPrice">£0.00</p>
        </div>
        <a class="fullComp__button button button" href="#">Buy tickets</a>
        <a class="fullComp__terms" href="./terms.php">Terms & Conditions</a>
      </div>
    </section>

    <a class="fullComp__next" href="#">Next competition ></a>

  </main>

<?php include "footer.php"; ?>
