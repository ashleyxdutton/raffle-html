</div>

<footer class="siteFooter" style="padding:30px;border-top:1px solid black;">
  <nav class="siteFooter__nav">
    <ul class="siteFooter__navList flex flex--x-between">
      <li class="siteFooter__navItem">
        <a class="siteFooter__navLink" href="./faqs.php">FAQ's</a>
      </li>
      <li class="siteFooter__navItem">
        <a class="siteFooter__navLink" href="./terms.php">T&C's</a>
      </li>
      <li class="siteFooter__navItem">
        <a class="siteFooter__navLink" href="./contact.php">Contact us</a>
      </li>
      <li class="siteFooter__navItem">
        <p class="siteFooter__navLink">Copyright 2020 PryzBox</p>
      </li>
    </ul>
  </nav>
</footer>

<script src="assets/js/modules.min.js"></script>
<script src="assets/js/scripts.min.js"></script>

</body>
</html>
